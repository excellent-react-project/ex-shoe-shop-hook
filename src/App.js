import logo from "./logo.svg";
import "./App.css";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import ExShoeShop from "./Ex_shoe_shop_hook/ExShoeShop";


function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<ExShoeShop />}></Route>
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
