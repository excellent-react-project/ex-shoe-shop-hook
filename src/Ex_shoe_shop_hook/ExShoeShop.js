import React, { useState, useEffect } from "react";
import Cart from "./Cart.js";
import { dataShoe } from "./data_shoe.js";
import ListShoe from "./ListShoe.js";

export default function ExShoeShop(props) {
  const [listShoe, setListShoe] = useState(dataShoe);
  const [cart, setCart] = useState([]);

  useEffect(() => {
    const filteredCart = cart.filter((item) => item.soLuong > 0);
    setCart(filteredCart);
  }, [cart]);

  const handleAddToCart = (shoe) => {
    let cloneCart = [...cart];
    let index = cloneCart.findIndex((item) => {
      return item.id == shoe.id;
    });

    if (index === -1) {
      let newShoe = { ...shoe, soLuong: 1 };
      cloneCart.push(newShoe);
    } else {
      cloneCart[index].soLuong++;
    }

    setCart(cloneCart);
  };

  return (
    <div className="container">
      <h2>{props.children}</h2>
      <Cart cart={cart} onCartChange={setCart} />
      <ListShoe handleAddToCart={handleAddToCart} list={listShoe} />
    </div>
  );
}
